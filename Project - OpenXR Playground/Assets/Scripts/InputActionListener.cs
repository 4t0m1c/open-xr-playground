using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class InputActionListener : MonoBehaviour {

    [SerializeField] InputActionReference actionRef;
    [SerializeField] UnityEvent<InputAction.CallbackContext> OnInputPerformed = new UnityEvent<InputAction.CallbackContext> ();

    void Start () {
        actionRef.action.Enable ();
        actionRef.action.performed += x => OnInputPerformed.Invoke (x);
    }

    void OnDestroy () {
        actionRef.action.Dispose ();
    }
}