// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/XRInputs.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @XRInputs : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @XRInputs()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""XRInputs"",
    ""maps"": [
        {
            ""name"": ""XR"",
            ""id"": ""4f0efecd-b9ca-4df8-876e-eb1259287e50"",
            ""actions"": [
                {
                    ""name"": ""ScreenPosition"",
                    ""type"": ""Value"",
                    ""id"": ""6b883188-bfae-411b-a6de-a2e45d371498"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ScreenTrigger"",
                    ""type"": ""Value"",
                    ""id"": ""f86f1421-bb73-4467-8f2a-b894083d070a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a43c42ad-7610-4a54-9e98-7a39fb862ec7"",
                    ""path"": ""<Touchscreen>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScreenPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""572d1309-3749-44f9-8f5c-b18acf48ba64"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScreenPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2cb3684a-dfd6-4d2d-a76c-0b912af84ec4"",
                    ""path"": ""<Touchscreen>/primaryTouch/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScreenTrigger"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""71edb175-1525-4be4-8877-1f4ca04d0d1c"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ScreenTrigger"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // XR
        m_XR = asset.FindActionMap("XR", throwIfNotFound: true);
        m_XR_ScreenPosition = m_XR.FindAction("ScreenPosition", throwIfNotFound: true);
        m_XR_ScreenTrigger = m_XR.FindAction("ScreenTrigger", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // XR
    private readonly InputActionMap m_XR;
    private IXRActions m_XRActionsCallbackInterface;
    private readonly InputAction m_XR_ScreenPosition;
    private readonly InputAction m_XR_ScreenTrigger;
    public struct XRActions
    {
        private @XRInputs m_Wrapper;
        public XRActions(@XRInputs wrapper) { m_Wrapper = wrapper; }
        public InputAction @ScreenPosition => m_Wrapper.m_XR_ScreenPosition;
        public InputAction @ScreenTrigger => m_Wrapper.m_XR_ScreenTrigger;
        public InputActionMap Get() { return m_Wrapper.m_XR; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(XRActions set) { return set.Get(); }
        public void SetCallbacks(IXRActions instance)
        {
            if (m_Wrapper.m_XRActionsCallbackInterface != null)
            {
                @ScreenPosition.started -= m_Wrapper.m_XRActionsCallbackInterface.OnScreenPosition;
                @ScreenPosition.performed -= m_Wrapper.m_XRActionsCallbackInterface.OnScreenPosition;
                @ScreenPosition.canceled -= m_Wrapper.m_XRActionsCallbackInterface.OnScreenPosition;
                @ScreenTrigger.started -= m_Wrapper.m_XRActionsCallbackInterface.OnScreenTrigger;
                @ScreenTrigger.performed -= m_Wrapper.m_XRActionsCallbackInterface.OnScreenTrigger;
                @ScreenTrigger.canceled -= m_Wrapper.m_XRActionsCallbackInterface.OnScreenTrigger;
            }
            m_Wrapper.m_XRActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ScreenPosition.started += instance.OnScreenPosition;
                @ScreenPosition.performed += instance.OnScreenPosition;
                @ScreenPosition.canceled += instance.OnScreenPosition;
                @ScreenTrigger.started += instance.OnScreenTrigger;
                @ScreenTrigger.performed += instance.OnScreenTrigger;
                @ScreenTrigger.canceled += instance.OnScreenTrigger;
            }
        }
    }
    public XRActions @XR => new XRActions(this);
    public interface IXRActions
    {
        void OnScreenPosition(InputAction.CallbackContext context);
        void OnScreenTrigger(InputAction.CallbackContext context);
    }
}
