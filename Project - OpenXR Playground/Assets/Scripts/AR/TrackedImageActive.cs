using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class TrackedImageActive : MonoBehaviour {
    ARTrackedImageManager m_TrackedImageManager;

    [SerializeField] GameObject modelPoca, modelRafiki;

    string scannedName;

    void Awake () {
        m_TrackedImageManager = GameObject.FindObjectOfType<ARTrackedImageManager> ();
    }

    void OnEnable () => m_TrackedImageManager.trackedImagesChanged += OnChanged;

    void OnDisable () => m_TrackedImageManager.trackedImagesChanged -= OnChanged;

    void OnChanged (ARTrackedImagesChangedEventArgs eventArgs) {
        foreach (var newImage in eventArgs.added) {
            // Handle added event
            if (scannedName.Length == 0) {
                Debug.Log ($"Tracked Image Added");
                scannedName = newImage.referenceImage.name;

                if (newImage.referenceImage.name == "poca") {
                    modelPoca.SetActive (true);
                } else if (newImage.referenceImage.name == "rafiki") {
                    modelRafiki.SetActive (true);
                }
            }
        }

        foreach (var updatedImage in eventArgs.updated) {
            // Handle updated event
            Debug.Log ($"Tracked Image Updated");
        }

        foreach (var removedImage in eventArgs.removed) {
            // Handle removed event
            Debug.Log ($"Tracked Image Removed");
        }
    }
}