using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputControllerXR : MonoBehaviour {

    public static UnityEvent<XRMode> OnXRModeSet = new UnityEvent<XRMode> ();
    public static UnityEvent OnScreenTriggerStarted = new UnityEvent ();
    public static UnityEvent OnScreenTriggerCanceled = new UnityEvent ();
    public static UnityEvent<Vector2> OnScreenPositionPerformed = new UnityEvent<Vector2> ();

    public enum XRMode {
        VR,
        AR
    }

    [SerializeField] XRMode xrMode;

    XRInputs xrInputs;

    void Start () {
        OnXRModeSet.Invoke (xrMode);

        xrInputs = new XRInputs ();
        xrInputs.Enable ();

        xrInputs.XR.ScreenTrigger.started += x => OnScreenTriggerStarted.Invoke ();
        xrInputs.XR.ScreenTrigger.canceled += x => OnScreenTriggerCanceled.Invoke ();

        xrInputs.XR.ScreenPosition.performed += x => OnScreenPositionPerformed.Invoke (x.ReadValue<Vector2> ());
    }

    void OnDestroy () {
        xrInputs.Dispose ();
    }

}