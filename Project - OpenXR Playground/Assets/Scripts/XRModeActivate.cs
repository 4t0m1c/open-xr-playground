using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRModeActivate : MonoBehaviour {

    [SerializeField]InputControllerXR.XRMode desiredXRMode;

    void OnEnable () {
        InputControllerXR.OnXRModeSet.AddListener(XRMode);
    }

    void OnDisable () {
        InputControllerXR.OnXRModeSet.RemoveListener(XRMode);
    }

    void XRMode (InputControllerXR.XRMode xRMode) {
        gameObject.SetActive(desiredXRMode == xRMode);
    }

}