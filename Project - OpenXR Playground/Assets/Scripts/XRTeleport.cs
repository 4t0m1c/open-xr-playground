using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRTeleport : MonoBehaviour {
    Vector3 worldPosition;
    Vector2 screenPosition;
    new Camera camera;

    [SerializeField] LayerMask rayLayers;
    [SerializeField] Transform positionPreview;
    [SerializeField] Transform playerTransformAR;
    [SerializeField] Transform playerTransformVR;

    void Start () {
        camera = Camera.main;
    }

    void OnEnable () {
        InputControllerXR.OnScreenTriggerStarted.AddListener (ScreenTriggerStarted);
        InputControllerXR.OnScreenPositionPerformed.AddListener (ScreenPositionPerformed);
        InputControllerXR.OnScreenTriggerCanceled.AddListener (ScreenTriggerCanceled);
    }

    void OnDisable () {
        InputControllerXR.OnScreenTriggerStarted.RemoveListener (ScreenTriggerStarted);
        InputControllerXR.OnScreenPositionPerformed.RemoveListener (ScreenPositionPerformed);
        InputControllerXR.OnScreenTriggerCanceled.RemoveListener (ScreenTriggerCanceled);
    }

    void ScreenTriggerStarted () {
        positionPreview.gameObject.SetActive (true);
    }

    void ScreenPositionPerformed (Vector2 screenPosition) {
        this.screenPosition = screenPosition;
        Raycast (camera.ScreenPointToRay (screenPosition));
    }

    void ScreenTriggerCanceled () {
        MoveToPosition ();
    }

    public void HandTriggerRay (Transform hand) {
        Raycast (new Ray (hand.position, hand.forward));
    }

    void Raycast (Ray ray) {
        if (Physics.Raycast (ray, out RaycastHit hit, 100, rayLayers, QueryTriggerInteraction.Ignore)) {
            worldPosition = hit.point;
            positionPreview.position = worldPosition;
        }
    }

    void MoveToPosition () {
        positionPreview.gameObject.SetActive (false);

        StopAllCoroutines ();
        StartCoroutine (MoveToPoint (worldPosition));
    }

    IEnumerator MoveToPoint (Vector3 endPosition) {
        float progress = 0;
        Vector3 startPostion = playerTransformAR.position;
        while (progress < 1) {
            progress += Time.deltaTime * 10;
            playerTransformAR.position = Vector3.Lerp (startPostion, endPosition, progress);
            playerTransformVR.position = Vector3.Lerp (startPostion, endPosition, progress);
            yield return null;
        }
    }
}